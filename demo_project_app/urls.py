from django.urls import path
from . import views

urlpatterns = [
    path('', views.get_index_page, name='get index'),
    path('get-nth-fibonacci', views.get_nth_fibonacci, name='get nth fibonacci'),
]