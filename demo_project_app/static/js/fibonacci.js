$(document).ready(function(){

    $(document).on('click', '#get-nth-term', function(){
        let nth_term = $('#nth-term-input').val();
        get_nth_trem(nth_term);
    }); // get series button


    function get_nth_trem(nth_term){
        $('#spinner').html('<i class="fa fa-spinner"></i>');
        $('#serise-block').html('');
        let ajaxTime= new Date().getTime();
        $.ajax({
            url: "/get-nth-fibonacci",
            type: "POST",
            dataType: "json",
            data: {nth_term: nth_term},
            success: function(data) {
                if(data['success']){
                let totalTime = new Date().getTime()-ajaxTime;
                $('#serise-block').html('<div><h5>Series:</h5> '+data['series']+'</div><br><div><h5>Nth term:</h5> \
                '+data['nth_term']+'</div><br><div><h5>Response time:</h5> '+totalTime+' ms</div>')
                }
                else{
                    $('#serise-block').html('<div class="invalid-feedback d-block">'+data['Message']+'</div>');
                }
                $('#spinner').html('');
            },
            error: function(data, textStatus, jqXHR) {
                
            }
        });
    } // get series, nth term
   
});