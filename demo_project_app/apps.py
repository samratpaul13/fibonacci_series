from django.apps import AppConfig


class DemoProjectAppConfig(AppConfig):
    name = 'demo_project_app'
