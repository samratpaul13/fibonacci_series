from django.db import models

class Fibonacci(models.Model):
    no_of_term = models.IntegerField(default=0)
    fibonacci_series = models.TextField(null=True, blank=True)
    nth_term = models.IntegerField(default=0)
