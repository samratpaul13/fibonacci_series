from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from .models import Fibonacci
from django.http import JsonResponse

@csrf_exempt
def get_index_page(request):
    return render(request, 'index.html', {})


@csrf_exempt
def get_nth_fibonacci(request):
    nth_term = request.POST.get('nth_term','0')
    if not nth_term.isdigit() or int(nth_term) < 1:
        return JsonResponse({'Message': 'Invalid input', 'success': False})
    fibonacci_obj = Fibonacci.objects.filter(no_of_term=nth_term).first()
    if fibonacci_obj:
        series = fibonacci_obj.fibonacci_series
        last_term = fibonacci_obj.nth_term
    else:
        series = get_fibonacci_series(int(nth_term))
        last_term = series[-1]
        series = ", ".join(series)
        fibonacci_obj = Fibonacci()
        fibonacci_obj.no_of_term = nth_term
        fibonacci_obj.fibonacci_series = series
        fibonacci_obj.nth_term = last_term
        fibonacci_obj.save()
    return JsonResponse({'series': series, 'nth_term': last_term, 'success': True})


def get_fibonacci_series(nterms):
    n1 = 0
    n2 = 1
    count = 0
    series = []
    while count < nterms:
        nth = n1 + n2
        n1 = n2
        n2 = nth
        count += 1
        series.append(str(n1))
    return series